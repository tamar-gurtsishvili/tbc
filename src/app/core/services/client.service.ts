import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../../../environments/environment';

import { IClient } from '../models/client';
import { IAccount } from '../models/account';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  apiURL: string;

  constructor(private http: HttpClient) {

    this.apiURL = environment.apiURL;
  }

  getClients(): Observable<IClient[]> {

    const fullRoute = this.apiURL + 'clients';

    return this.http.get<IClient[]>(fullRoute);
  }

  getClient(clientID: number): Observable<IClient> {

    const fullRoute = this.apiURL + 'client/' + clientID;

    return this.http.get<IClient>(fullRoute);
  }

  getClientAccounts(clientID: number): Observable<IAccount[]> {

    const fullRoute = this.apiURL + `client/${clientID}/accounts`;

    return this.http.get<IAccount[]>(fullRoute);
  }

  updateClient(client: IClient): Observable<{ status: string }> {

    const fullRoute = this.apiURL + 'client/update';

    return this.http.put<{ status: string }>(fullRoute, client);
  }

  createClient(client: IClient): Observable<{ status: string }> {

    const fullRoute = this.apiURL + 'client/create';

    return this.http.post<{ status: string }>(fullRoute, client);
  }

  deleteClient(clientID: number): Observable<{ status: string, data: IClient[] }> {

    const fullRoute = this.apiURL + 'client/delete/' + clientID;

    return this.http.delete<{ status: string, data: IClient[] }>(fullRoute);
  }
}
