import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';

import { IAccount } from '../models/account';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  apiURL: string;

  constructor(private http: HttpClient) {
    this.apiURL = environment.apiURL;
  }

  closeAccount(accountID: number): Observable<{ status: string, data: IAccount[] }> {

    const fullRoute = this.apiURL + 'account/close/' + accountID;

    return this.http.get<{ status: string, data: IAccount[] }>(fullRoute);
  }

  createAccount(account: Account): Observable<{ status: string, data: Account[] }> {

    const fullRoute = this.apiURL + 'account/create';

    return this.http.post<{ status: string, data: Account[] }>(fullRoute, account);
  }
}
