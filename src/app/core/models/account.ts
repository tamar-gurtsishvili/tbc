export interface IAccount {
  id: number;
  clientID: number;
  type: string;
  currency: string;
  accountStatus: number;
}
