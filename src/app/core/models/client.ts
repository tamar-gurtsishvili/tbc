import { IAddress } from './address';

export interface IClient {
  id: number;
  firstName: string;
  lastName: string;
  gender: string;
  personalID: string;
  mobile: string;
  legalAddress: IAddress;
  physicalAddress: IAddress;
  photo: string;
}
