import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

import { IClient } from '../models/client';
import { ClientService } from '../services/client.service';

@Injectable({
  providedIn: 'root'
})
export class ClientsResolver implements Resolve<IClient[]> {

  constructor(private clientService: ClientService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IClient[]> | Promise<IClient[]> | IClient[] {

    return this.clientService.getClients();
  }
}
