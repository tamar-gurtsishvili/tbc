import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

import { ClientService } from '../services/client.service';
import { IAccount } from '../models/account';

@Injectable({
  providedIn: 'root'
})
export class ClientAccountsResolver implements Resolve<IAccount[]> {

  constructor(private clientService: ClientService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IAccount[]> | Promise<IAccount[]> | IAccount[] {

    return this.clientService.getClientAccounts(route.params.id);
  }
}
