import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ClientsComponent } from './pages/clients/clients.component';
import { ClientDetailsComponent } from './pages/client-details/client-details.component';
import { ClientCreateComponent } from './pages/client-create/client-create.component';

import { ClientsResolver } from 'src/app/core/resolvers/clients-resolver.service';
import { ClientResolver } from 'src/app/core/resolvers/client-resolver.service';
import { ClientAccountsResolver } from 'src/app/core/resolvers/client-accounts-resolver.service';

const routes: Routes = [
  {
    path: '',
    component: ClientsComponent,
    resolve: { clients: ClientsResolver }
  },
  {
    path: 'create',
    component: ClientCreateComponent
  },
  {
    path: ':id',
    component: ClientDetailsComponent,
    resolve: { client: ClientResolver, accounts: ClientAccountsResolver }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientsRoutingModule { }
