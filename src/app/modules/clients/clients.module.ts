import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { ClientsRoutingModule } from './clients-routing.module';

import { ClientsComponent } from './pages/clients/clients.component';
import { ClientFormComponent } from './components/client-form/client-form.component';
import { ClientCreateComponent } from './pages/client-create/client-create.component';
import { ClientDetailsComponent } from './pages/client-details/client-details.component';
import { AccountFormComponent } from './components/account-form/account-form.component';

import { TableModule } from 'primeng/table';
import { TabViewModule } from 'primeng/tabview';
import { DialogModule } from 'primeng/dialog';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { DropdownModule } from 'primeng/dropdown';
import { RadioButtonModule } from 'primeng/radiobutton';
import { TriStateCheckboxModule } from 'primeng/tristatecheckbox';
import { FileUploadModule } from 'primeng/fileupload';

@NgModule({
  declarations: [
    ClientsComponent,
    ClientFormComponent,
    ClientCreateComponent,
    ClientDetailsComponent,
    AccountFormComponent
  ],
  exports: [
      ClientsComponent
  ],
  imports: [
    CommonModule,
    ClientsRoutingModule,
    ReactiveFormsModule,
    TableModule,
    TabViewModule,
    DialogModule,
    ConfirmDialogModule,
    ButtonModule,
    InputTextModule,
    RadioButtonModule,
    DropdownModule,
    TriStateCheckboxModule,
    FileUploadModule
  ],
  providers: [
    ConfirmationService
  ],
})
export class ClientsModule { }
