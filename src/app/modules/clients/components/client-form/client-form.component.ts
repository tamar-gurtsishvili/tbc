import { Component, Input, OnInit, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IClient } from '../../../../core/models/client';
import { ClientService } from '../../../../core/services/client.service';

@Component({
  selector: 'app-client-form',
  templateUrl: './client-form.component.html',
  styleUrls: ['./client-form.component.css']
})
export class ClientFormComponent implements OnInit, OnChanges {

  @Input() client: IClient;
  @Output() onClientFormSubmit = new EventEmitter();

  clientForm = new FormGroup({
    id: new FormControl(null, Validators.pattern('[0-9]*')),
    firstName: new FormControl(null, [
      Validators.required,
      Validators.minLength(2),
      Validators.maxLength(50),
      this.forbiddenNameValidator
    ]),
    lastName: new FormControl(null, [
      Validators.required,
      Validators.minLength(2),
      Validators.maxLength(50),
      this.forbiddenNameValidator
    ]),
    gender: new FormControl('male', Validators.required),
    personalID: new FormControl(null, [
      Validators.required,
      Validators.pattern('[0-9]{11}')
    ]),
    mobile: new FormControl(null, [
      Validators.required,
      Validators.pattern('[5][0-9]{8}')
    ]),
    legalAddress: new FormGroup({
      country: new FormControl(null, Validators.required),
      city: new FormControl(null, Validators.required),
      address: new FormControl(null, Validators.required),
    }),
    physicalAddress: new FormGroup({
      country: new FormControl(null, Validators.required),
      city: new FormControl(null, Validators.required),
      address: new FormControl(null, Validators.required),
    }),
    photo: new FormControl(null),
  });

  constructor(private clientService: ClientService) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {

    if (changes.client && this.client) {

      this.clientForm.setValue(this.client);
    }
  }

  clientFormSubmit() {

    if (this.clientForm.valid) {

      if (this.client && this.client.id) {
        this.updateClient();
      } else {
        this.createClient();
      }
    }
  }

  updateClient() {

    this.clientService.updateClient(this.clientForm.value)
      .subscribe(
        (data) => {

          this.onClientFormSubmit.emit(data.status);
        }
      );
  }

  createClient() {

    this.clientService.createClient(this.clientForm.value)
      .subscribe(
        (data) => {

          this.clientForm.reset();
          this.onClientFormSubmit.emit(data.status);
        }
      );
  }

  onClientPhotoSelect(event) {

    if (event.files[0]) {
      this.clientForm.get('photo').setValue(event.files[0]);
    }
  }

  forbiddenNameValidator(control: FormControl): { [key: string]: boolean } | null {

    const geoPattern = /^[ა-ჰ]+$/;
    const engPattern = /^[a-zA-Z]+$/;

    if (!geoPattern.test(control.value) && !engPattern.test(control.value)) {
      return { forbiddenName: true };
    }
    return null;
  }
}

