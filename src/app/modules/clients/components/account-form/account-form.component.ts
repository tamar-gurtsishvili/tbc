import { Component, Input, Output, OnInit, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { AccountService } from 'src/app/core/services/account.service';

@Component({
  selector: 'app-account-form',
  templateUrl: './account-form.component.html',
  styleUrls: ['./account-form.component.css']
})
export class AccountFormComponent implements OnInit, OnChanges {

  @Input() clientID: number;
  @Input() formResetRequest: boolean;
  @Output() onAccountFormSubmit = new EventEmitter();

  accountTypes = [
    {name: 'Current', code: 'current'},
    {name: 'Saving', code: 'saving'},
    {name: 'Accumulative', code: 'accumulative'}
  ];

  accountCurrencies = [
    { name: 'GEL', code: 'GEL' },
    { name: 'USD', code: 'USD' },
    { name: 'EUR', code: 'EUR' },
    { name: 'RUB', code: 'RUB' }
  ];

  accountForm: FormGroup;

  constructor(private accountService: AccountService) { }

  ngOnInit() {

    this.accountForm = new FormGroup({
      id: new FormControl(null, Validators.pattern('[0-9]*')),
      clientID: new FormControl(this.clientID, [
        Validators.required,
        Validators.pattern('[0-9]*')
      ]),
      type: new FormControl(null, Validators.required),
      currency: new FormControl(null, Validators.required),
      accountStatus: new FormControl(1, Validators.required),
    });
  }

  ngOnChanges(changes: SimpleChanges) {

    if (changes.formResetRequest && this.formResetRequest) {
      this.resetAccountForm();
    }
  }

  accountFormSubmit() {

    if (this.accountForm.valid) {

      let a = this.accountForm.value;
      a.type = a.type.code;
      a.currency = a.currency.code;
      a.accountStatus = a.accountStatus ? 1 : 0;

      this.accountService.createAccount(a)
        .subscribe(
          (data) => {

            this.resetAccountForm();
            this.onAccountFormSubmit.emit(data);
          }
        );
    }
  }

  resetAccountForm() {

    this.accountForm.reset({ clientID: this.clientID, accountStatus: 1 });
  }
}
