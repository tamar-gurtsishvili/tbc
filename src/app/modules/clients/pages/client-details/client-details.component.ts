import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Data, Router } from '@angular/router';

import { Message } from 'primeng/api';

import { IClient } from '../../../../core/models/client';
import { ClientService } from '../../../../core/services/client.service';
import { IAccount } from '../../../../core/models/account';
import { AccountService } from 'src/app/core/services/account.service';

@Component({
  selector: 'app-client',
  templateUrl: './client-details.component.html',
  styleUrls: ['./client-details.component.css']
})
export class ClientDetailsComponent implements OnInit {

  client: IClient;
  clientAccounts: IAccount[];

  showAccountFormDialog = false;

  statusMsg: Message[] = [];

  resetAccountForm = false;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private clientService: ClientService,
              private accountService: AccountService) { }

  ngOnInit() {

    this.route.data
      .subscribe(
        (data: Data) => {
          this.client = data.client;
          this.clientAccounts = data.accounts;
        }
      );
  }

  closeClientAccount(accountID: number) {

    this.accountService.closeAccount(accountID)
      .subscribe(
        (data) => {

          this.showStatusMsg(data.status);
          this.clientAccounts = data.data;
        }
      );
  }

  onAccountFormSubmit(event) {

    this.showStatusMsg(event.status);
    this.clientAccounts = event.data;
    this.toggleAccountFormDialog();
  }

  toggleAccountFormDialog() {

    this.showAccountFormDialog = !this.showAccountFormDialog;
  }

  showStatusMsg(msg) {

    this.statusMsg = [{ severity: 'info', detail: msg }];
  }

  onAccountFormDialogShow() {

    this.resetAccountForm = true;

    setTimeout(() => { this.resetAccountForm = false; }, 250);
  }
}
