import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Data, Router } from '@angular/router';

import { Table } from 'primeng/table';
import { ConfirmationService, Message } from 'primeng/api';

import { IClient } from '../../../../core/models/client';
import { ClientService } from '../../../../core/services/client.service';

interface QueryParams {
  page: number;
  sort: string | Array<any>;
  filter: string | object;
}

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {
  clients: IClient[];

  @ViewChild('dt')
  table: Table;

  columns: any[];
  rowsPerPage = 5;
  rowsSortMeta: QueryParams;

  routeQueryParams: QueryParams;

  statusMsg: Message[] = [];

  constructor(private route: ActivatedRoute,
              private router: Router,
              private clientService: ClientService,
              private confirmationService: ConfirmationService) { }

  ngOnInit() {

    this.rowsSortMeta = {
      page: 0,
      sort: null,
      filter: {}
    };

    this.routeQueryParams = {
      page: null,
      sort: null,
      filter: null
    };

    this.columns = [
      { field: 'id', header: 'Client Number' },
      { field: 'firstName', header: 'First Name' },
      { field: 'lastName', header: 'Last Name' },
      { field: 'personalID', header: 'Personal ID' },
      { field: 'details', header: 'Details' },
      { field: 'delete', header: 'Delete' },
    ];

    this.route.data
      .subscribe(
        (data: Data) => {
          this.clients = data.clients;
        }
      );

    this.rowsDefaultSortByQueryParams(this.route.snapshot.queryParams);
  }

  rowsDefaultSortByQueryParams(params) {

    const p = +params.page || 0;
    this.rowsSortMeta.page = p * this.rowsPerPage;
    this.routeQueryParams.page = p;

    if (params.filter) {

      let f;
      try {
        f = JSON.parse(params.filter);
      } catch (e) {
        return;
      }

      this.rowsSortMeta.filter = f;
      this.routeQueryParams.filter = params.filter;
    }

    if (params.sort) {

      let s;
      try {
        s = JSON.parse(params.sort);
      } catch (e) {
        return;
      }

      this.rowsSortMeta.sort = s;
      this.routeQueryParams.sort = params.sort;
    }
  }

  onPageChange(event) {

    this.routeQueryParams.page = event.first / this.rowsPerPage;
    this.router.navigate(['/clients'], { queryParams: this.routeQueryParams });
  }

  onFilter(event) {

    this.routeQueryParams.filter = JSON.stringify(event.filters);
    this.router.navigate(['/clients'], { queryParams: this.routeQueryParams });
  }

  onSort(event) {

    this.routeQueryParams.sort = JSON.stringify(event.multisortmeta);
    this.router.navigate(['/clients'], { queryParams: this.routeQueryParams });
  }

  resetTable() {

    this.table.reset(); // sort-ზე არ მუშაობს ?!

    this.routeQueryParams = { page: null, sort: null, filter: null };
    this.router.navigate(['/clients'], { queryParams: this.routeQueryParams });
  }

  clientDeleteConfirmation(clientID: number) {

    this.confirmationService.confirm({
      message: 'Do you want to delete this record?',
      header: 'Delete Confirmation',
      icon: 'fa fa-skull-crossbones',
      accept: () => {
        this.deleteClient(clientID);
      },
      reject: () => {
        this.statusMsg = [{ severity: 'info', summary: 'Rejected', detail: 'You have rejected' }];
      }
    });
  }

  deleteClient(clientID: number) {

    this.clientService.deleteClient(clientID)
      .subscribe(data => {

        this.clients = data.data;

        this.statusMsg = [{ severity: 'info', summary: 'Confirmed', detail: data.status }];
      });
  }
}
