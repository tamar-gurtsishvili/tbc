import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';

import { Message } from 'primeng/api';

@Component({
  selector: 'app-client-create',
  templateUrl: './client-create.component.html',
  styleUrls: ['./client-create.component.css']
})
export class ClientCreateComponent implements OnInit {

  statusMsg: Message[] = [];

  constructor(private router: Router) { }

  ngOnInit() {
  }

  showStatusMsg(msg) {

    this.statusMsg = [{ severity: 'info', detail: msg }];

    setTimeout(() => {
      this.router.navigate(['/clients']);
    }, 1000);
  }

}
